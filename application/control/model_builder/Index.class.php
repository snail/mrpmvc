<?php  if(!defined('IN_APP')){exit();}
#控制器
class Index extends BaseControl{
      public function onIndex(){
        $this->view->display('model_builder/index.php');
      }
      public function onReadInfo(){
             
            if($this->db->getVar('select count(*) from '.P('table'))===FALSE){
                 exit('表  '.P('table').' 不存在.');
             }else{
                 if($row=$this->db->row('select * from '.P('table'))){
                 if(!isset($row[P('pk')])){ exit('主键'.P('pk').'不存在。');}
                  $rule=array();
                  foreach ($row as $col=>$value) {
                      $rule[]=$col;
                      if(P('pk')==$col)continue;
                      $rule2[]=$col;
                  }
                  $data['rule_add']=$rule2;
                  $data['rule_modify']=$rule;
                  $data['attach']=P();
                  $this->view->display('model_builder/show_create_model.php',$data);
                 }else{
                      exit('请保证表'.P('table').'至少存在一条记录.');
                 }
        }
      }
     public function onShowCreateAction(){
              $row=$this->db->row('select * from '.P('table'));
              foreach ($row as $col=>$value) {
                  $rule[]=$col;
              }
              $data['rule']=$rule;
              $data['attach']=array('table'=>P('table'),'pk'=>P('pk'),'model'=>P('model'));
              $this->view->display('model_builder/show_create_action.php',$data);
      }
      public function onShowCreateView(){
              $data['attach']=P();
              $this->view->display('model_builder/show_create_view.php',$data);
      }
      public function onCreateModel(){
                      $keys=array();
                      $rule=array();
                      $rule2=array();
                      $map=array();
                      $row=$this->db->row('select * from '.P('table'));
                      foreach (array_keys($row) as $col) {
                          if(!(trim(P($col.'_modify_reg'),'  '))){
                              $rule2[$col]=array('rule'=>P($col.'_add_reg'),'msg'=>P($col.'_add_hint'));
                          }else{
                              $rule2[$col]=array('rule'=>P($col.'_modify_reg'),'msg'=>P($col.'_modify_hint'));
                          }
                          $map[$col]=$col;
                          if($col===P('pk'))continue;
                          $keys[]=$col;
                          $rule[$col]=array('rule'=>P($col.'_add_reg'),'msg'=>P($col.'_add_hint'));
                      }
                      $keys=$this->formatArray(var_export($keys,true));
                      
                      $rule=var_export($rule,true);
                      $rule=str_replace("\n", "\n".str_repeat(' ',26), $rule);
                      
                      $rule2=var_export($rule2,true);
                      $rule2=str_replace("\n", "\n".str_repeat(' ',26), $rule2);
                      
                      $map=var_export($map,true);
                      $map=str_replace("\n", "\n".str_repeat(' ',26), $map);
                      
                      $tpl=file_get_contents(APP_ROOT.'view/model_builder/TplModel.php');
                      
                      $data=str_replace("'#keys#'", $keys, $tpl);
                      $data=str_replace("AtableA", P('table'), $data);
                      $data=str_replace("AmodelA", P('model'), $data);
                      $data=str_replace("ApkA", P('pk'), $data);
                      $data=str_replace("Apk2A", ucfirst(P('pk')), $data);
                      $data=str_replace("'#rule#'", $rule, $data);
                      $data=str_replace("'#rule2#'", $rule2, $data);
                      $data=str_replace("'#map#'", $map, $data);
                      //echo ($data);
                      force_download(P('model').'Model.class.php',$data);
      }
      public function onCreateAction(){
                 $map=array();
                 $name=P('name');
                 $type=P('type');
                 $col=P('col');
                 foreach($col as $key=>$val){
                        if($type[$key]){
                           $map[]=array('col'=>$val,'name'=>($name[$key]?$name[$key]:$val),'type'=>$type[$key]);
                        }
                 }
                $map=$this->formatArray(var_export($map,true));
                $tpl=file_get_contents(APP_ROOT.'view/model_builder/TplAction.php');
                $data=str_replace("AcontrolA", P('action_name'), $tpl);
                $data=str_replace("'#map#'",$map,$data);
                $data=str_replace("AtableA",P('table'),$data);
                $data=str_replace("ApkA",P('pk'),$data);
                $data=str_replace("ApageColsA",implode(',',$col),$data);
                force_download(P('action_name').'.class.php',$data);
      }
      public function onCreateAddView(){
             $data=array();$data["rows"]=array();
             $data['table']=P('table');
             $data['pk']=P('pk');
             $name=P('name');
             $type=P('type');
             $col=P('col');
             foreach($col as $key=>$val){
                    if($type[$key]){
                       $data["rows"][]=array('col'=>$val,'name'=>($name[$key]?$name[$key]:$val),'type'=>$type[$key]);
                    }
             }
             $ret=$this->view->display('model_builder/TplAdd.php',$data,true);
             $ret=str_replace("&{",'[{', $ret);
             $ret=str_replace("}&", '}]', $ret);
             //echo $ret;
             force_download(P('table').'_add.php',$ret);
      }
      public function onCreateModifyView(){
             $data=array();$data["rows"]=array();
             $data['table']=P('table');
             $data['pk']=P('pk');
             $name=P('name');
             $type=P('type');
             $col=P('col');
             foreach($col as $key=>$val){
                    if($type[$key]){
                       $data["rows"][]=array('col'=>$val,'name'=>($name[$key]?$name[$key]:$val),'type'=>$type[$key]);
                    }
             }
             $tpl=$this->view->display('model_builder/TplModify.php',$data,true);
             $data=str_replace("&{",'[{', $tpl);
             $data=str_replace("}&", '}]', $data);
             force_download(P('table').'_modify.php',$data);
      }
      public function onCreateListView(){
             $data=array();$data["rows"]=array();
             $data['table']=P('table');
             $data['pk']=P('pk');
             $name=P('name');
             $type=P('type');
             $col=P('col');
             foreach($col as $key=>$val){
                    if($type[$key]){
                       $data["rows"][]=array('col'=>$val,'th'=>($name[$key]?$name[$key]:$val),'type'=>$type[$key]);
                    }
             }
             $tpl=$this->view->display('model_builder/TplList.php',$data,true);
             $tpl=str_replace("#{",'<!--<', $tpl);
             $tpl=str_replace("}#", '>-->', $tpl);
             $tpl=str_replace("[&{",'[{', $tpl);
             $tpl=str_replace("}&]", '}]', $tpl);
             force_download(P('table').'_list.php',$tpl);
      }
      private function formatArray($str){
             $patterns = array (
                    "/\n/"
                   ,"/ /"
                   ,"/\\d+=>/"
                   );
             $replace = array (
                    ""
                   ,""
                   ,""
                   );
            return preg_replace ($patterns, $replace,$str);
          
      }
}