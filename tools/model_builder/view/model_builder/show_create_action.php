<!--< if(!defined('IN_APP')){exit();}>--><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MrPmvc模型生成工具</title>
[{script('js/jq/j')}]
<script>
$(function(){
     var $form=$('#data_form');
     $('#createAction').click(function(){
         if(!$('input[name="action_name"]').val()){alert('请填写控制器名称。');return;}
         $form.attr({'target':'hide','action':'?c=model_builder.Index&m=createAction'});
         $form.submit();
     });
     $('#createAddView').click(function(){
         if(!$('input[name="action_name"]').val()){alert('请填写控制器名称。');return;}
         $form.attr({'target':'hide','action':'?c=model_builder.Index&m=CreateAddView'});
         $form.submit();
     });
     $('#createMdifyView').click(function(){
         if(!$('input[name="action_name"]').val()){alert('请填写控制器名称。');return;}
         $form.attr({'target':'hide','action':'?c=model_builder.Index&m=CreateModifyView'});
         $form.submit();
     });
     $('#createListView').click(function(){
         if(!$('input[name="action_name"]').val()){alert('请填写控制器名称。');return;}
         $form.attr({'target':'hide','action':'?c=model_builder.Index&m=CreateListView'});
         $form.submit();
     });
      $('#next').click(function(){
         $form.attr({'target':'_self','action':'?c=model_builder.Index&m=ShowCreateView'});
         $form.submit();
      });
});
</script>
<style type="text/css">
body{padding:40px;}
th,td{padding:10px;}
legend{color:green;font-size:16px;font-weight:bold;}
fieldset{border:2px solid #cdcdcd;margin-bottom:20px;}
input[type=button]{cursor:pointer;}
td{text-align:center;}
</style>
</head>
<body>
<form id="data_form" action="?c=model_builder.Index&m=ReadInfo" method="post" target="hide">
<fieldset>
<legend>控制器信息</legend>
<table cellpadding="0" cellspacing="0" >
     <tr>
      <td>控制器类名</td>
     <td><input type="text" name="action_name" value="[{$attach['model']}]"/>.class.php</td>
     </tr>
</table>
</fieldset>
<fieldset>
<legend>字段表单显示形式</legend>
<hr class="hr1"/>
<table cellpadding="0" cellspacing="0" >
<tr><th>字段名</th><th>显示形式</th><th>表单(或列表页表头)提示名称</th></tr>
<!--< foreach($rule  as $col){>-->
<tr>
    <td>[{$col}]</td>
    <td><select name="type[]" class="zdy">
        <option value="">不显示</option>
        <option value="text">文本框</option>
        <option value="textarea">文本域</option>
        <option value="textareahtml">富文本域</option>
        <option value="hidden">隐藏域</option>
        <option value="zdy">稍后自定义</option>
        </select>
    </td>
    <td><input type="text" name="name[]"/><input type="hidden" name="col[]" value="[{$col}]"/></td>
</tr>
<!--< }//end foreach>-->
</table>
</fieldset>
<!--< foreach($attach as $key=>$value){>-->
<input type="hidden" value="[{$value}]" name="[{$key}]"/>
<!--< }//end foreach>-->
</form>
<fieldset>
<legend>生成操作</legend>
<table cellpadding="0" cellspacing="0" >
     <tr>
     <td>
     <input type="button" id="createAction" value="1.生成控制器"/>
     <input type="button" id="createAddView" value="2.生成添加视图"/>
     <input type="button" id="createMdifyView" value="3.生成修改视图"/>
     <input type="button" id="createListView" value="4.生成列表视图"/>
    <!--input type="button" id="next" value="下一步"/-->
    </td>
     </tr>
</table>
</fieldset>
    <iframe name="hide" style="display:none;"></iframe>
</body>
</html>