<?php  if(!defined('IN_APP')){exit();}
/**
 * MrPmvc默认视图类
 * @author 狂奔的蜗牛
 * @email  672308444@163.com
 * @version alpha
 */
class BaseView{
       public function display($filename,$data=null,$return=false){
           $file=APP_ROOT.'view/'.$filename;
           $cache=array();
           if(is_object($data)){
                foreach($data as $key=>$val){
                   $cache[$key]=$val;
                }
                $data=$cache;
           }
           if(is_array($data))extract($data,EXTR_OVERWRITE);
           #返回数据吗
           if($return){ob_start();}
           if(file_exists($file)){
                 $contents=file_get_contents(realpath($file));
                 #模板预处理
                 $contents=str_replace('[{','<?php echo ',$contents);
                 $contents=str_replace('}]',';?>',$contents);
                 $contents=str_replace('<!--<','<?php ',$contents);
                 $contents=str_replace('>-->','?>',$contents);
                 #执行模板
                 eval('?>'.$contents);
           }else{
                notFound('View : <b>'.$filename.'</b>');
           }
           #返回数据吗
           if($return){$contents=ob_get_contents();ob_clean(); return $contents;}
       }
}