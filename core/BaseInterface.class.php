<?php

if (!defined('IN_APP')) {
    exit();
}

/**
 * MrPmvc用户自定义数据库类、缓存类、视图类,接口类
 * @author 狂奔的蜗牛
 * @email  672308444@163.com
 * @version alpha
 */
class BaseInterface {

    private static $cache = null;
    private static $db = null;
    private static $view = null;

    #自定义缓存类接口，请在方法getCacheObj中，根据传入的配置（config.inc.php），返回一个缓存操作类实例化对象,
    #返回null不使用缓存

    public static function &getCacheObj($cfg) {
        if (is_object(self::$cache)) {
            return self::$cache;
        } else {
            #缓存类对象实例化应该在下面进行
            //return self::$cache;#不加载缓存操作类
            self::$cache = new Cache(CACHE_DIR, '');
            return self::$cache;
        }
    }

    #自定义数据库类接口类，请在方法getDBObj中，根据传入的配置（config.inc.php），返回一个数据库操作类实例化对象。
    #返回null不加载数据库操作类

    public static function &getDBObj($cfg) {
        if (is_object(self::$db)) {
            //实例化过就直接返回
            return self::$db;
        } else {
            //首次实例化
            #数据库类对象实例化应该在下面进行
            //return self::$db;#不加载数据库操作类
            self::$db = new BaseDB($cfg[$cfg['db_dsn_default']], $cfg['db_user'], $cfg['db_pass']) or die('Connect Database error.');
            if ($cfg['db_dsn_default'] == 'db_dsn_mysql') {
                self::$db->exec('set names utf8');
            }
            return self::$db;
        }
    }

    #自定义视图接口类，请在方法getViewObj中，根据传入的配置（config.inc.php），返回一个视图操作类实例化对象,
    #返回null使用默认视图BaseView

    public static function &getViewObj($cfg) {
        if (is_object(self::$view)) {
            return self::$view;
        } else {
            #视图类对象实例化应该在下面进行
            return self::$view; //使用默认视图BaseView
            $work_dir = CACHE_DIR . 'smarty';
            self::$view = new Smarty();
            self::$view->debugging = false;
            self::$view->caching = false;
            self::$view->left_delimiter = '<!--{';
            self::$view->right_delimiter = '}-->';
            self::$view->cache_lifetime = 120;
            self::$view->template_dir = APP_ROOT . 'view/';
            self::$view->compile_dir = $work_dir . '/templates_c/';
            self::$view->config_dir = $work_dir . '/configs/';
            self::$view->cache_dir = $work_dir . '/cache/';
            return self::$view;
        }
    }

}