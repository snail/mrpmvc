<?php

if (!defined('IN_APP')) {
    exit();
}

/**
 * MrPmvc基础控制器 和 基础模型 的共同父类
 * @author 狂奔的蜗牛
 * @email  672308444@163.com
 * @version alpha
 */
class BaseUtil {

    protected $db = null;
    protected $cache = null;
    private $cfg_file;

    public function __construct() {
        #配置文件路径名称
        $this->cfg_file = APP_ROOT . 'config.inc.php';
        #初始化数据库，使用接口返回的自定义数据库对象,否则设为null
        $cfg = $this->getConfig();
        if ($cfg['db_auto_load']) {
            $this->db = BaseInterface::getDBOBJ($cfg);
        }
        #初始化缓存，使用接口返回的自定义缓存对象,否则设为null
        $cache = BaseInterface::getCacheOBJ($this->getConfig());
        $this->cache = is_object($cache) ? $cache : null;
    }

    #写入配置项到配置文件

    public function writeConfig($cfg) {
        if (!is_array($cfg)) {
            return;
        }
        $cfg_file = $this->cfg_file;
        $header = "<?php #所有的配置都写在这里\nreturn ";
        $config = $this->getConfig();
        $config = $config == null ? array() : $config;
        foreach ($cfg as $key => $val) {
            $config[$key] = $val;
        }
        $header.=var_export($config, TRUE) . ';';
        file_put_contents($cfg_file, $header);
    }

    #删除配置文件中的配置项

    public function deleteConfig($key) {
        if (!$key) {
            return;
        }
        $cfg_file = $this->cfg_file;
        $header = "<?php #所有的配置都写在这里\nreturn ";
        $config = $this->getConfig();
        $config = $config == null ? array() : $config;
        if (isset($config[$key])) {
            unset($config[$key]);
        }
        $header.=var_export($config, TRUE) . ';';
        file_put_contents($cfg_file, $header);
    }

    #获得配置文件中的配置

    public function getConfig($key = '') {
        $cfg_file = $this->cfg_file;
        if (!file_exists($cfg_file)) {
            return null;
        }
        $config = include($cfg_file);
        if ($key && is_array($config) && isset($config[$key])) {
            return $config[$key];
        }
        if (!$key && is_array($config)) {
            return $config;
        }
        return null;
    }

    #手动加载数据库,用于数据库自动连接设为false时使用

    protected function initDB() {
        if (!is_object($this->db))
            $this->db = BaseInterface::getDBOBJ($this->getConfig());
    }

}