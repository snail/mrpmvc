<?php

#入口文件
define('APP_DIR_NAME', 'application');            #程序目录
define('SYS_DIR_NAME', 'core');                   #系统目录
define('DEFAULT_CONTROL', 'Index');               #默认控制器
define('DEFAULT_CONTROL_METHOD', 'Index');        #控制器默认方法
define('DEBUG', TRUE);                            #报错模式，false关闭报错
define('COUNT_RUNTIME', TRUE);                   #是否开启页面运行时间统计，开启后可能会损失一点点性能。
#在视图中通过{RUNTIME}显示运行时间，单位秒。{MEMORY}显示使用的内存，单位KB。前提是COUNT_RUNTIME为TRUE
define('AUTH_KEY', 'itisasecret');                #字符串加密解密函数的密码，使用时一定要设置为别人不知道的密码
#默认时区
date_default_timezone_set('PRC');

require(SYS_DIR_NAME . '/SystemFunction.class.php');



if (!DEBUG) {
    error_reporting(0);
} else {
    error_reporting(E_ALL);
}



define('IN_APP', TRUE);


$file_path = $_SERVER['SCRIPT_NAME'];
$arr = explode('/', $file_path);
#入口文件名称                              例如：index.php
define('ENTRANCE_FILE_NAME', $arr[count($arr) - 1]);

#入口文件所在目录                          例如：D:/www/blog/
define('ENTRANCE_ROOT', str_replace(DIRECTORY_SEPARATOR, '/', dirname(__FILE__) . '/'));

#程序所在目录                              例如：D:/www/blog/app/
define('APP_ROOT', ENTRANCE_ROOT . APP_DIR_NAME . '/');
#系统所在目录                              例如：D:/www/blog/app/
define('SYS_ROOT', ENTRANCE_ROOT . SYS_DIR_NAME . '/');

#入口文件的url绝对路径（不含域名）         例如：/blog/index.php
define('ABS_ENTRANCE_URL', $_SERVER['SCRIPT_NAME']);

#入口文件所在目录的url绝对路径（不含域名） 例如：/blog/
define('ABS_ENTRANCE_PATH', str_replace(ENTRANCE_FILE_NAME, '', ABS_ENTRANCE_URL));

#入口文件的url路径（含域名）               例如：http://127.0.0.1/blog/index.php
define('ENTRANCE_URL', 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME']);

#入口文件所在目录的url路径（含域名）       例如：http://127.0.0.1/blog/
define('ENTRANCE_PATH', str_replace(ENTRANCE_FILE_NAME, '', ENTRANCE_URL));

#资源目录定位                              例如：http://127.0.0.1/blog/res/,也可以是http://形式，用于在视图中加载js和css文件
define('RESOURCE_LINK', ENTRANCE_PATH . 'res/');

#缓存目录定位                              例如：D:/www/blog/cache/，用于程序运行时存放各种缓存文件
define('CACHE_DIR', ENTRANCE_ROOT . 'cache/');

#网站访问日志开关                          用于开启和关闭记录站点访问信息 TRUE关闭日志记录 FALSE开启日志记录
define('SITE_LOG_IS_STOP', TRUE);

#网站访问日志目录定位                      例如：D:/www/blog/cache/log/，用于存放站点访问日志文件
define('SITE_LOG_SAVE_PATH', CACHE_DIR . 'log/');



SystemFunction::init();

